#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
from pandas import DataFrame


class DataParser:
    """Parser służacy do przygotowywania obiektu typu DataFrame z tekstowych źródeł danych """

    NEWLINE = "\n"

    def __init__(self, root, sources):
        """
            Inicjuje ścieżki do katalogu z danymi

            :param str root: ścieżka do głównego katalogu z źródłami danych
            :param list sources: lista folderów zawierających źródła danych oraz nazwy klas `file_class`, do których należą
        """
        self.root = root
        self.sources = sources

    def read_files(self, path):
        """
            Generator pobierający zawartość kolejnych plików z podanego katalogu

            :param str path: ścieżka do folderu
            :returns zawartość `content` i ściezka do pliku `file_path`
        """
        for root, dir_names, file_names in os.walk(path):
            for file_name in file_names:
                file_path = os.path.join(root, file_name)
                if os.path.isfile(file_path):
                    with open(file_path, "r", encoding="latin-1") as file:
                        content = file.read()

                    yield file_path, content

    def generate_data_frame(self):
        """
            Pobiera i przetwarza dane z katalogu

            :returns zwraca `DataFrame`
        """
        data = DataFrame({'text': [], 'file_class': []})
        for path, file_class in self.sources:
            folder_path = os.path.join(self.root, path)
            rows = []
            index = []
            for file_name, text in self.read_files(folder_path):
                rows.append({'text': text, 'file_class': file_class})
                index.append(file_name)
            data = data.append(DataFrame(rows, index=index))
        return data
