#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy
from pandas import DataFrame
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn.model_selection import train_test_split


class NaiveBayes:
    """ Wielomianowy Naiwny klasyfikator bayesowski z wykorzystaniem biblioteki Scikit learn """

    def __init__(self, data, test_size, use_idf):
        """
            Dzieli przekazane dane na próbki uczące i testowe i inicjuje parametry klasyfikatora

            :param DataFrame data: Dane
            :param float test_size: proporcja próbki testowej względem uczącej
            :param bool use_idf: czy wykorzystać wektorowanie czestości występownaia słów z zastowoasniem narzędzia Tidf
        """
        if use_idf:
            self.vectorizer = TfidfVectorizer()
        else:
            self.vectorizer = CountVectorizer()
        self.classifier = MultinomialNB()

        self.trainData, self.testData = train_test_split(data, test_size=test_size, random_state=0)

    def train(self):
        """
            Uczy wielomianowy klasyfikator bayesa na przygotowanej próbce danych
        """
        counts = self.vectorizer.fit_transform(self.trainData['text'].values)
        targets = self.trainData['file_class'].values
        self.classifier.fit(counts, targets)

    def validate(self):
        """
            Dokonuje walidacji klasyfikatora na podstawie przygotowanej próbki uczącej

             :returns zwraca spakowane tablice zawierajace predykcje i klasy właściwe
        """
        counts = self.vectorizer.transform(self.testData['text'].values)
        predictions = self.classifier.predict(counts)
        return predictions, self.testData['file_class'].values
