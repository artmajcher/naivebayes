#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    ROOT - ścieżka do głownego katalogu z danymi
    SOURCES - lista nazw podkatalogów zawierajacych dane wraz z nawami ich klas
    TEST_SAMPLE_SIZE - % danych testowych z ogółu danych
    USE_TIDF - czy wkorzystać wektorowanie Tfidf, domyślnie zliczane są wystąpienia słów (CountVectorizer)
"""
from naive_bayes.DataParser import DataParser
from naive_bayes.NaiveBayes import NaiveBayes
from sklearn.metrics import confusion_matrix, classification_report

TEST_SAMPLE_SIZE = .2
USE_TIDF = False
ROOT = "C:/Users/artur/Desktop/python/naive_bayes/data/"
SOURCES = [
    ('biology/', 'BIOLOGY'),
    ('computer/', 'COMPUTER'),
    ('earth/', 'EARTH'),
    ('france/', 'FRANCE')
]


def main():
    """


    """
    print("Data parsing ...")
    print("Source:", ROOT)
    parser = DataParser(ROOT, SOURCES)
    data = parser.generate_data_frame()
    print("Total:", len(data))

    print("Training ...")
    nb = NaiveBayes(data, TEST_SAMPLE_SIZE, USE_TIDF)
    nb.train()

    print("Validating ...")
    print("Test sample size:", TEST_SAMPLE_SIZE * 100, "%")
    predictions, expected = nb.validate()
    print("\n")

    print("Summary:")
    print(classification_report(expected, predictions))
    print('Confusion matrix:')
    print(confusion_matrix(expected, predictions))


if __name__ == "__main__":
    main()
