==============================
Naiwny klasyfikator bayesowski
==============================

Naiwny klasyfikator bayesowski to prosty klasyfikator probabilistyczny. Naiwne klasyfikatory bayesowskie są oparte na założeniu
o wzajemnej niezależności predyktorów (zmiennych niezależnych). Często nie mają one żadnego związku z rzeczywistością i właśnie
z tego powodu nazywa się je naiwnymi. Bardziej opisowe jest określenie – „model cech niezależnych”. Ponadto model prawdopodobieństwa
można wyprowadzić korzystając z twierdzenia Bayesa.


Uruchamianie
~~~~~~~~~~~~~~~~~~~~~~~~~

Konfiguracja ścieżek do plików odbywa poprzez zmienne w skypcie ``classifier.py``

.. code-block:: python

  ROOT = "C:/Users/artur/Desktop/python/naive_bayes/data/"
  SOURCES = [
    ('science/', 'SCIENCE'),
    ('sea/', 'SEA')
    ...
  ]

Wywołanie :code:`python classifier.py` wykonuje algorytm


Wywołanie komendy :code:`python setup.py install` doda punkt startowy ``nb`` do aktualnego środowiska. Jego wywołanie
uruchomi skonfigurowany algorytm


Testy
~~~~~~~~~~~~~~~~~~~~~~~~~

Do testów wykorzystano podzbiór 4 klas dokumentów tekstowych ze zbioru:  .. _dane:https://www.dropbox.com/s/4jfjiuue46wxcux/text_files.tar.gz?dl=0
Próbka ucząca wynosiła 20%


Wyniki przy wykorzystaniu wektorowania czestości występownaia słów z zastowoasniem narzędzia Tidf ``sklearn.feature_extraction.text.TfidfVectorizer``

+------------+------------+-----------+----------+-----------+
|            |  precision |  recall   | f1-score |  support  |
+============+============+===========+==========+===========+
| BIOLOGY    |   0.81     |   0.66    |  0.73    |  2002     |
+------------+------------+-----------+----------+-----------+
| COMPUTER   |   0.75     |   0.80    |  0.78    |  1971     |
+------------+------------+-----------+----------+-----------+
| EARTH      |   0.76     |   0.69    |  0.72    |  2015     |
+------------+------------+-----------+----------+-----------+
| FRANCE     |   0.68     |   0.83    |  0.75    |  1984     |
+------------+------------+-----------+----------+-----------+
| avg /total |   0.75     |    0.74   |   0.74   |   7972    |
+------------+------------+-----------+----------+-----------+

Macież pomyłek:

+------------+------------+-----------+---------+
| 1320       |   183      |      279  |  220    |
+------------+------------+-----------+---------+
| 47         |   1577     |      73   |  220    |
+------------+------------+-----------+---------+
| 184        |   167      |      1391 |  273    |
+------------+------------+-----------+---------+
| 69         |   168      |      99   |  1648   |
+------------+------------+-----------+---------+


Wyniki przy wykorzystaniu zlicznia słów ``sklearn.feature_extraction.text.CountVectorizer``


+------------+------------+-----------+----------+-----------+
|            |  precision |  recall   | f1-score |  support  |
+============+============+===========+==========+===========+
| BIOLOGY    |   0.76     |   0.70    |  0.73    |  2002     |
+------------+------------+-----------+----------+-----------+
| COMPUTER   |   0.80     |   0.74    |  0.77    |  1971     |
+------------+------------+-----------+----------+-----------+
| EARTH      |   0.74     |   0.70    |  0.72    |  2015     |
+------------+------------+-----------+----------+-----------+
| FRANCE     |   0.67     |    0.82   |   0.74   |  1984     |
+------------+------------+-----------+----------+-----------+
| avg /total |   0.74     |    0.74   |   0.74   |   7972    |
+------------+------------+-----------+----------+-----------+

Macież pomyłek:

+------------+------------+-----------+---------+
| 1393       |   121      |      269  |  219    |
+------------+------------+-----------+---------+
| 98         |   1463     |      94   |  316    |
+------------+------------+-----------+---------+
| 239        |   106      |      1402 |  268    |
+------------+------------+-----------+---------+
| 92         |   139      |      125  |  1628   |
+------------+------------+-----------+---------+





Wymagane biblioteki
~~~~~~~~~~~~~~~~~~~~~~~~~
* numpy
* pandas
* scikit-learn


Autorzy
~~~~~~~~~~~~~~~~~~~~~~~~~

* Artur Majcher
* Piotr Rajtar
* Jakub Kulikowski

Note
~~~~~~~~~~~~~~~~~~~~~~~~~

This project has been set up using PyScaffold 2.5.6. For details and usage
information on PyScaffold see http://pyscaffold.readthedocs.org/.